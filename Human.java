public class Human {
    private String name;
    private int money;
    private int hunger;
    private int boredom;

    public Human(String name){
        this.name = name;
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
    }
public boolean doWork(){
    if (this.hunger < 50 && this.boredom < 50){
        this.money = money + 20;
        this.hunger = hunger + 5;
        this.boredom = boredom + 10;
        System.out.println("The human worked successfully.");
        return true;
    }
    else if (this.hunger > 50) {
        System.out.println("The human was too hungry to work.");
        return false;
    }
    else {
        System.out.println("The human was too bored to work.");
        return false; 
    }
}

public void play() {
 
    this.boredom = this.boredom - 30;

    if (this.boredom <= 0 ) {
        this.boredom = 0;
    }

    this.hunger = this.hunger + 5;
    System.out.println("The human played with the dog");
}
}