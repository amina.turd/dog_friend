/**
 *author: Amina Turdalieva 
 * id: 2035572
 * date: 2021-09-07
 */

public class Dog {
    
    private String name;
    private int energy;
    private int hunger;
    private int boredom;

    public Dog(String name) {
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean takeNap() {
        if (this.hunger < 50 && this.boredom < 50) {

            this.energy = this.energy + 20;
            this.hunger = this.hunger + 5;
            this.boredom =this.boredom + 10; 

            System.out.println("The dog napped succesfully");

            return true;

        } else if(this.hunger > 50) {

            System.out.println("The dog was too hangry to nap"); 

            return false;

        } else {

            System.out.println("The dog was too bored to nap");

            return false;
        }


    }

    public void play() {
 
        this.boredom = this.boredom - 30;
    
        if (this.boredom <= 0 ) {
            this.boredom = 0;
        }
    
        this.hunger = this.hunger + 5;
        System.out.println("The dog played with the human");
    }

    public boolean playWith(Human person) {

        if(this.energy > 50) {

            this.energy = this.energy - 50;
            play();
            person.play();
            return true;

        } else {

            System.out.println("The dog doesn't have enough energy");

            return false;
        } 
    }
}
